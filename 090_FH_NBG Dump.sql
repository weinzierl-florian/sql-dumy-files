-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 02. Jun 2018 um 23:34
-- Server Version: 5.5.39
-- PHP-Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `fh_nbg`
--
CREATE DATABASE IF NOT EXISTS `fh_nbg` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `fh_nbg`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `hoert`
--

DROP TABLE IF EXISTS `hoert`;
CREATE TABLE IF NOT EXISTS `hoert` (
  `MatrNr` int(11) NOT NULL,
  `VorlNr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `hoert`
--

INSERT INTO `hoert` (`MatrNr`, `VorlNr`) VALUES
(25403, 5001),
(26120, 5001),
(26120, 5045);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `professor`
--

DROP TABLE IF EXISTS `professor`;
CREATE TABLE IF NOT EXISTS `professor` (
  `PersNr` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `professor`
--

INSERT INTO `professor` (`PersNr`, `Name`) VALUES
(12, 'Wirth'),
(15, 'Tesla'),
(20, 'Holliday');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `student`
--

DROP TABLE IF EXISTS `student`;
CREATE TABLE IF NOT EXISTS `student` (
  `MatrNr` int(11) NOT NULL,
  `Name` varchar(30) NOT NULL,
  `Vorname` varchar(30) NOT NULL,
  `Str` varchar(30) NOT NULL,
  `PLZ` varchar(5) NOT NULL,
  `Ort` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `student`
--

INSERT INTO `student` (`MatrNr`, `Name`, `Vorname`, `Str`, `PLZ`, `Ort`) VALUES
(25403, 'Jonas', 'Jan', 'Enge Gasse', '90704', 'Nürnberg'),
(26120, 'Fichte', 'Klaus', 'Breite Basse 7', '90704', 'Nürnberg'),
(27103, 'Fäuler', 'Tom', 'Fürther Str. 120', '90762', 'Fürth');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `vorlesung`
--

DROP TABLE IF EXISTS `vorlesung`;
CREATE TABLE IF NOT EXISTS `vorlesung` (
  `VorlNr` int(11) NOT NULL,
  `PersNr` int(11) NOT NULL,
  `Titel` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `vorlesung`
--

INSERT INTO `vorlesung` (`VorlNr`, `PersNr`, `Titel`) VALUES
(5001, 15, 'ET'),
(5022, 12, 'IT'),
(5045, 12, 'DB');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hoert`
--
ALTER TABLE `hoert`
 ADD PRIMARY KEY (`MatrNr`,`VorlNr`), ADD KEY `VorlNr` (`VorlNr`);

--
-- Indexes for table `professor`
--
ALTER TABLE `professor`
 ADD PRIMARY KEY (`PersNr`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
 ADD PRIMARY KEY (`MatrNr`);

--
-- Indexes for table `vorlesung`
--
ALTER TABLE `vorlesung`
 ADD PRIMARY KEY (`VorlNr`,`PersNr`), ADD KEY `PersNr` (`PersNr`);

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `hoert`
--
ALTER TABLE `hoert`
ADD CONSTRAINT `hoert_ibfk_1` FOREIGN KEY (`VorlNr`) REFERENCES `vorlesung` (`VorlNr`);

--
-- Constraints der Tabelle `vorlesung`
--
ALTER TABLE `vorlesung`
ADD CONSTRAINT `vorlesung_ibfk_1` FOREIGN KEY (`PersNr`) REFERENCES `professor` (`PersNr`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
