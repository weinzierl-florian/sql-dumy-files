-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 10. Apr 2013 um 13:46
-- Server Version: 5.5.27
-- PHP-Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `wawi_gross`
--
CREATE DATABASE IF NOT EXISTS `wawi_gross` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `wawi_gross`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `artikel`
--

CREATE TABLE IF NOT EXISTS `artikel` (
  `A_Nr` int(11) NOT NULL DEFAULT '0',
  `A_Bez` varchar(50) DEFAULT NULL,
  `A_Art` varchar(50) DEFAULT NULL,
  `A_VK` decimal(19,2) DEFAULT '0.00',
  `A_Bestand` int(11) DEFAULT '0',
  `A_MinBestand` int(11) DEFAULT '0',
  PRIMARY KEY (`A_Nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `artikel`
--

INSERT INTO `artikel` (`A_Nr`, `A_Bez`, `A_Art`, `A_VK`, `A_Bestand`, `A_MinBestand`) VALUES
(1, 'Brother HL 2700', 'Laserdrucker', 999.00, 15, 2),
(2, 'Epson C1100', 'Laserdrucker', 474.00, 4, 1),
(3, 'Belinea 1019', 'TFT', 310.00, 10, 4),
(4, 'Acer AL1716', 'TFT', 250.00, 7, 5),
(5, 'TEAC DV-28', 'DVD-Laufwerk', 89.00, 20, 5),
(6, 'Sony DDU16', 'DVD-Laufwerk', 25.00, 15, 4),
(7, 'LG GDR-81', 'DVD-Laufwerk', 35.00, 8, 4);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bestelldetails`
--

CREATE TABLE IF NOT EXISTS `bestelldetails` (
  `B_Nr` int(11) NOT NULL DEFAULT '0',
  `A_Nr` int(11) NOT NULL DEFAULT '0',
  `B_Menge` int(11) DEFAULT '0',
  PRIMARY KEY (`B_Nr`,`A_Nr`),
  KEY `A_Nr` (`A_Nr`),
  KEY `B_Nr` (`B_Nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bestelldetails`
--

INSERT INTO `bestelldetails` (`B_Nr`, `A_Nr`, `B_Menge`) VALUES
(1, 1, 7),
(1, 3, 2),
(1, 4, 1),
(2, 5, 1),
(3, 6, 4),
(4, 7, 2),
(5, 3, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bestellungen`
--

CREATE TABLE IF NOT EXISTS `bestellungen` (
  `B_Nr` int(11) NOT NULL DEFAULT '0',
  `B_Datum` datetime DEFAULT NULL,
  `B_Lieferdatum` datetime DEFAULT NULL,
  `B_Erledigt` tinyint(4) DEFAULT '0',
  `B_Rechnung` tinyint(4) DEFAULT '0',
  `K_Nr` int(11) DEFAULT '0',
  PRIMARY KEY (`B_Nr`),
  KEY `K_Nr` (`K_Nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `bestellungen`
--

INSERT INTO `bestellungen` (`B_Nr`, `B_Datum`, `B_Lieferdatum`, `B_Erledigt`, `B_Rechnung`, `K_Nr`) VALUES
(1, '2004-09-23 00:00:00', '2004-10-07 00:00:00', -1, -1, 5),
(2, '2004-09-23 00:00:00', '2004-10-10 00:00:00', -1, -1, 4),
(3, '2005-09-23 00:00:00', '2005-10-01 00:00:00', 0, 0, 3),
(4, '2005-09-24 00:00:00', '2005-10-14 00:00:00', 0, 0, 6),
(5, '2005-09-25 00:00:00', '2005-10-16 00:00:00', 0, 0, 2),
(6, '2005-10-20 00:00:00', '2005-10-26 00:00:00', 0, 0, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kunden`
--

CREATE TABLE IF NOT EXISTS `kunden` (
  `K_Nr` int(11) NOT NULL DEFAULT '0',
  `K_Name` varchar(30) DEFAULT NULL,
  `K_Vorname` varchar(20) DEFAULT NULL,
  `K_Strasse` varchar(30) DEFAULT NULL,
  `K_PLZ` varchar(5) DEFAULT NULL,
  `K_Ort` varchar(30) DEFAULT NULL,
  `K_Telefon` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`K_Nr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `kunden`
--

INSERT INTO `kunden` (`K_Nr`, `K_Name`, `K_Vorname`, `K_Strasse`, `K_PLZ`, `K_Ort`, `K_Telefon`) VALUES
(1, 'Gruber', 'Claus', 'Karol 11', '90763', 'Fürth', '0911-774499'),
(2, 'Weber', 'Gerd', 'Kanne 27', '90768', 'Fürth', '0911-779987'),
(3, 'Ullrich', 'Antonia', 'Haupt 11', '90844', 'Nürnberg', '0911-844544'),
(4, 'Kohlmann', 'Hartmut', 'Birne 1', '45555', 'Nürnberg', '0445-777777'),
(5, 'Hintersteiner', 'Xaver', 'Platz 8', '89000', 'München', '089-4444444'),
(6, 'Lohmeier', 'Karin', 'Hohlweg 18', '89445', 'München', '089-445588');

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `bestelldetails`
--
ALTER TABLE `bestelldetails`
  ADD CONSTRAINT `bestelldetails_ibfk_1` FOREIGN KEY (`B_Nr`) REFERENCES `bestellungen` (`B_Nr`),
  ADD CONSTRAINT `bestelldetails_ibfk_2` FOREIGN KEY (`A_Nr`) REFERENCES `artikel` (`A_Nr`);

--
-- Constraints der Tabelle `bestellungen`
--
ALTER TABLE `bestellungen`
  ADD CONSTRAINT `bestellungen_ibfk_1` FOREIGN KEY (`K_Nr`) REFERENCES `kunden` (`K_Nr`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
